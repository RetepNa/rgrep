use clap::{Arg, Command};
use colorful::{Color, Colorful};
use rgrep::Matcher;
use std::io::{self, BufRead};

fn main() {
    let matches = Command::new("RGREP")
        .version("1.0")
        .about("Simple grep alternative")
        .arg(
            Arg::new("pattern")
                .required(true)
                .index(1)
                .takes_value(true),
        )
        .arg(Arg::new("line-numbers").long("line-number").short('n'))
        .arg(Arg::new("ignore-case").long("ignore-case").short('i'))
        .arg(Arg::new("dont-highlight").long("dont-highlight").short('l'))
        .arg(Arg::new("print-all").long("print-all").short('p'))
        .get_matches();

    let ignore_case = matches.is_present("ignore-case");
    let pattern = matches.value_of("pattern").unwrap();
    let show_line_count = matches.is_present("line-numbers");
    let dont_highlight = matches.is_present("dont-highlight");
    let print_all = matches.is_present("print-all");
    let stdin = io::stdin();

    let mut current_line_count = 0;

    for item in stdin.lock().lines() {
        let mut line = item.unwrap_or_default().clone();
        if ignore_case {
            line = line.to_lowercase();
        }

        let local_pattern = if ignore_case {
            Matcher::new(&pattern.to_string().to_lowercase())
        } else {
            Matcher::new(&pattern.to_string())
        };

        let found = local_pattern.matches(&line);
        if found > (0 as usize) || print_all{
            if show_line_count {
                let number_show = format!("{}:", current_line_count);
                print!("{} ", number_show.color(Color::Blue));
            }
            if !dont_highlight {
                let line_splits: Vec<&str> = line.split(&local_pattern.pattern_str()).collect();
                for i in 0..line_splits.len() {
                    let split = line_splits[i];
                    print!("{}", &split);
                    if (i + 1) == line_splits.len() {
                        print!("\n");
                        continue;
                    }
                    print!("{}", local_pattern.pattern_str().color(Color::Red));
                }
            } else {
                println!("{}", &line);
            }
        }
        current_line_count += 1;
    }
}
