#[derive(Debug, Clone, PartialEq, Default)]
pub struct Matcher {
    pattern: Vec<State>,
    pattern_as_str: String,
}
#[derive(Debug, Clone, PartialEq)]
struct State {
    next_char: Option<char>,
    has_wildchart: bool,
}

impl Matcher {
    pub fn new(pattern: &str) -> Matcher {
        let mut tmp_vec: Vec<State> = Vec::with_capacity(pattern.len());
        let mut was_star = false;
        for item in pattern.chars() {
            match item {
                '*' => {
                    was_star = true;
                }
                _ => {
                    let s = State {
                        next_char: Some(item),
                        has_wildchart: was_star,
                    };
                    tmp_vec.push(s);
                    was_star = false;
                }
            }
        }

        if !pattern.is_empty() {
            let final_state = State {
                next_char: None,
                has_wildchart: was_star,
            };
            tmp_vec.push(final_state);
        }

        Matcher {
            pattern: tmp_vec,
            pattern_as_str: pattern.clone().to_owned(),
        }
    }
    pub fn pattern_str(&self) -> String {
        self.pattern_as_str.clone()
    }
    pub fn pattern_len(&self)->usize{
        self.pattern.len()
    }
    pub fn matches_with_wildcart(&self, input: &str) -> bool {
        if self.pattern.is_empty() {
            return input.is_empty();
        }
        let mut current_index = 0;
        for input_char in input.chars() {
            match self.pattern.get(current_index) {
                None => return false,
                Some(st) if st.next_char == Some('?') || st.next_char == Some(input_char) => {
                    current_index += 1;
                }
                Some(st) if st.has_wildchart => {
                    if st.next_char == None {
                        return true;
                    }
                }
                _ => {
                    if current_index == 0 {
                        return false;
                    }
                    current_index -= 1;
                    while let Some(pattern) = self.pattern.get(current_index) {
                        if pattern.has_wildchart {
                            if pattern.next_char == Some('?')
                                || pattern.next_char == Some(input_char)
                            {
                                current_index += 1;
                            }
                            break;
                        }
                        if current_index == 0 {
                            return false;
                        }
                        current_index -= 1;
                    }
                }
            }
        }
        self.pattern[current_index].next_char.is_none()
    }
    pub fn matches(&self, input: &str) -> usize {
        if let Some(index) = input.find(&self.pattern_as_str) {
            return index;
        }
        return 0 as usize;
    }
}

impl<'a> PartialEq<&'a str> for Matcher {
    fn eq(&self, other: &&'a str) -> bool {
        self.matches_with_wildcart(other)
    }
}
